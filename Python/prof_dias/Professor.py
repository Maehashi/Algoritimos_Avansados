import csv

professores = []

def busca_professor(professor):
   res = [prof for prof in professores if prof['Nome'] == professor]
   return res

with open('aulacerta.csv', encoding='utf-8-sig') as ficheiro:
        reader = csv.reader(ficheiro, delimiter=';')
        for row in reader:
            professor = row[0]
            materia = row[1]
            numeroDeAulas = row[2]
            diaDisponivel = row[3]
            hora_1 = row[4]
            hora_2 = row[5]

            prof = busca_professor(professor)
            if not prof:
                professores.append({"Nome": professor,   "Dados": {"DaAulaDe": {materia: numeroDeAulas}, 
                "DiasDisponiveis": {diaDisponivel: [hora_1,hora_2]}}})
            else:
                prof[0]["Dados"]["DiasDisponiveis"].update({diaDisponivel: [hora_1, hora_2]})
print(professores)