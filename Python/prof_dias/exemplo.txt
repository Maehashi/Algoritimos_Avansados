


[{'dia': 'quarta', 'aulas': [], 'professores-disponiveis': ['Victor', 'Cesar']}, 
{'dia': 'quinta', 'aulas': [], 'professores-disponiveis': ['Victor']}, 
{'dia': 'terça', 'aulas': [], 'professores-disponiveis': ['Ettore']}, 
{'dia': 'sexta', 'aulas': [], 'professores-disponiveis': ['Cleber']}, 
{'dia': 'segunda', 'aulas': [], 'professores-disponiveis': ['Faulin', 'Eloiza']}]


[{'Materia': 'Algoritmos Avançados', 'Dados': {'quantidade_aula': '6', 'leciondado-por': 'Victor'}}, 
{'Materia': 'Redes de Computadores', 'Dados': {'quantidade_aula': '4', 'leciondado-por': 'Ettore'}}, 
{'Materia': 'Web Semântica', 'Dados': {'quantidade_aula': '4', 'leciondado-por': 'Cleber'}}, 
{'Materia': 'Manejos Agrícolas de Tratos Culturais e Colheita', 'Dados': {'quantidade_aula': '2', 'leciondado-por': 'Faulin'}}, 
{'Materia': 'Empreendedorismo', 'Dados': {'quantidade_aula': '2', 'leciondado-por': 'Cesar'}}, 
{'Materia': 'Inglês', 'Dados': {'quantidade_aula': '2', 'leciondado-por': 'Eloiza'}}]


[{'Nome': 'Victor', 'Dados': {'leciona': 'Algoritmos Avançados', 'disonibilidade': {'quarta': ['13', '17'], 'quinta': ['13', '17']}}}, 
{'Nome': 'Ettore', 'Dados': {'leciona': 'Redes de Computadores', 'disonibilidade': {'terça': ['13', '17']}}}, 
{'Nome': 'Cleber', 'Dados': {'leciona': 'Web Semântica', 'disonibilidade': {'sexta': ['13', '17']}}}, 
{'Nome': 'Faulin', 'Dados': {'leciona': 'Manejos Agrícolas de Tratos Culturais e Colheita', 'disonibilidade': {'segunda': ['15', '17']}}}, 
{'Nome': 'Cesar', 'Dados': {'leciona': 'Empreendedorismo', 'disonibilidade': {'quarta': ['13', '15']}}}, 
{'Nome': 'Eloiza', 'Dados': {'leciona': 'Inglês', 'disonibilidade': {'segunda': ['13', '15']}}}]


[{'Nome': 'Victor', 'Dados': {'leciona': 'Algoritmos Avançados', 'disonibilidade': {'quarta': ['1,2,3,4'], 'quinta': ['1,2,3,4']}}}, 
{'Nome': 'Ettore', 'Dados': {'leciona': 'Redes de Computadores', 'disonibilidade': {'terça': ['1,2,3,4']}}}, 
{'Nome': 'Cleber', 'Dados': {'leciona': 'Web Semântica', 'disonibilidade': {'sexta': ['1,2,3,4']}}}, 
{'Nome': 'Faulin', 'Dados': {'leciona': 'Manejos Agrícolas de Tratos Culturais e Colheita', 'disonibilidade': {'segunda': ['3,4']}}}, 
{'Nome': 'Cesar', 'Dados': {'leciona': 'Empreendedorismo', 'disonibilidade': {'quarta': ['1,2']}}}, 
{'Nome': 'Eloiza', 'Dados': {'leciona': 'Inglês', 'disonibilidade': {'segunda': ['1,2']}}}]




{'dias': [{'dia': 'quarta', 'aulas': [], 'professores-disponiveis': ['Victor', 'Cesar']}, 
{'dia': 'quinta', 'aulas': [], 'professores-disponiveis': ['Victor']}, 
{'dia': 'terça', 'aulas': [], 'professores-disponiveis': ['Ettore']}, 
{'dia': 'sexta', 'aulas': [], 'professores-disponiveis': ['Cleber']}, 
{'dia': 'segunda', 'aulas': [], 'professores-disponiveis': ['Faulin', 'Eloiza']}], 
'professores': [{'Nome': 'Victor', 'Dados': {'leciona': 'Algoritmos Avançados', 'disonibilidade': {'quarta': ['13', '17'], 'quinta': ['13', '17']}}}, 
{'Nome': 'Ettore', 'Dados': {'leciona': 'Redes de Computadores', 'disonibilidade': {'terça': ['13', '17']}}}, 
{'Nome': 'Cleber', 'Dados': {'leciona': 'Web Semântica', 'disonibilidade': {'sexta': ['13', '17']}}}, 
{'Nome': 'Faulin', 'Dados': {'leciona': 'Manejos Agrícolas de Tratos Culturais e Colheita', 'disonibilidade': {'segunda': ['15', '17']}}}, 
{'Nome': 'Cesar', 'Dados': {'leciona': 'Empreendedorismo', 'disonibilidade': {'quarta': ['13', '15']}}}, 
{'Nome': 'Eloiza', 'Dados': {'leciona': 'Inglês', 'disonibilidade': {'segunda': ['13', '15']}}}], 
'materias': [{'Materia': 'Algoritmos Avançados', 'Dados': {'quantidade_aula': '6', 'leciondado-por': 'Victor'}}, 
{'Materia': 'Redes de Computadores', 'Dados': {'quantidade_aula': '4', 'leciondado-por': 'Ettore'}}, 
{'Materia': 'Web Semântica', 'Dados': {'quantidade_aula': '4', 'leciondado-por': 'Cleber'}}, 
{'Materia': 'Manejos Agrícolas de Tratos Culturais e Colheita', 'Dados': {'quantidade_aula': '2', 'leciondado-por': 'Faulin'}}, 
{'Materia': 'Empreendedorismo', 'Dados': {'quantidade_aula': '2', 'leciondado-por': 'Cesar'}}, 
{'Materia': 'Inglês', 'Dados': {'quantidade_aula': '2', 'leciondado-por': 'Eloiza'}}]}