# -- encoding: utf-8 --
'''
1 BUSCAR MELHORES PROFESSORES DO DIA (VAI RETORNAR UM VETO COM OS VERTICES DOS PROFESSORES EM ORDEM)
2 AGENDAR AULA NO DIA(VAI ENTRAR UM DIA E UMA LISTA DE POSSIVEIS PROFESSORES E ELE VAI AGENDAR AS POSSIVEIS AULAS)
3 UM LOOP QUE VAI DE SEGUNDA A SEXTA BUSCANDO OS PROFESSORES E AGENDANDO AS AULAS
'''
grafo = []

grafo.append({'segunda':{'aulas':[], 'professores-disponiveis':['eloisa','faulin']}})
grafo.append({'terca':{'aulas':[],'professores-disponiveis':['ettore','eloisa','cesar','faulin']}})
grafo.append({'quarta':{'aulas':[],'professores-disponiveis':['cesar','borba']}})
grafo.append({'quinta':{'aulas':[],'professores-disponiveis':['borba','faulin','eloisa']}})
grafo.append({'sexta':{'aulas':[],'professores-disponiveis':['cleber','cesar']}})

grafo.append({'eloisa':{'leciona':'ingles','disonibilidade':{'segunda':[1,2,3,4],'terca':[1,2,3,4],'quinta':[1,2,3,4]}}})
grafo.append({'faulin':{'leciona':'mato','disonibilidade':{'segunda':[1,2,3,4],'terca':[1,2,3,4]}}})
grafo.append({'cesar':{'leciona':'empreend','disonibilidade':{'terca':[1,2,3,4],'quarta':[1,2,3,4],'sexta':[1,2,3,4]}}})
grafo.append({'borba':{'leciona':'algoritimos','disonibilidade':{'quarta':[3,4],'quinta':[1,2,3,4]}}})
grafo.append({'ettore':{'leciona':'redes','disonibilidade':{'terca':[1,2,3,4],}}})
grafo.append({'cleber':{'leciona':'web_sem','disonibilidade':{'sexta':[1,2,3,4],'quinta':[3,4]}}})

grafo.append({'ingles':{'quantidade_aula': 2, 'leciondado-por':'eloisa'}})
grafo.append({'mato':{'quantidade_aula': 2, 'leciondado-por':'faulin'}})
grafo.append({'empreend':{'quantidade_aula': 2, 'leciondado-por':'cesar'}})
grafo.append({'algoritimos':{'quantidade_aula': 6, 'leciondado-por':'borba'}})
grafo.append({'redes':{'quantidade_aula': 4, 'leciondado-por':'ettore'}})
grafo.append({'web_sem':{'quantidade_aula': 4, 'leciondado-por':'cleber'}})
# print(grafo)


def buscarVertice(nome):
    for vertices in grafo:
        if nome in list(vertices.keys()):
            return vertices

def melhoresProfDia(dia):
    print('dia',dia)
    dadosDia = buscarVertice(dia)
    professores = []
    for prof in dadosDia[dia]['professores-disponiveis']:
        professores.append([prof])
    # return professores
    listaProfessores = professores[:]
    print('professorTotal',listaProfessores)
    for prof in listaProfessores:
        # print('prof',prof)
        verticeProf = buscarVertice(prof[0])
        mostraDispo = verticeProf[prof[0]]['disonibilidade']
        contaDispo = len(mostraDispo)        
        prof.append(contaDispo)
        print('professor1',professores)
    print('professor2',professores)
    professores = sorted(professores,key=lambda professores: professores[1])
    print('professor3',professores)

    return professores

def agendaAula(dia):
    verticeDia = buscarVertice(dia)
    print(verticeDia)
    auxAulas = verticeDia.get(dia)
    aulas = auxAulas.get('aulas')
    print(len(aulas))
    # while len(aulas) < 4:
    professoresNoDia = melhoresProfDia(dia)
    for professor in professoresNoDia:
        prof = professor[0]
        print(prof)
        verticeProf = buscarVertice(prof)
        auxProf = verticeProf.get(str(prof))
        print(auxProf)
        leciona = auxProf.get('leciona')
        print(leciona)
        disonibilidade = auxProf.get('disonibilidade')
        print(disonibilidade)
        verticeMateria = buscarVertice(str(leciona))
        print(verticeMateria)
        auxMateria = verticeMateria.get(str(leciona))
        quantidadeAula = auxMateria.get('quantidade_aula')
        
        # AQUI E' ONDE AS AULAS SAO ADICIONADAS NOS DIAS.
        try:
            while quantidadeAula > 0 and len(aulas) < 4 :
                aulas.append(leciona)
                quantidadeAula = quantidadeAula - 1
            try:
                auxMateria.update(quantidade_aula = quantidadeAula)
            except:
                pass
            try:
                disonibilidade.pop(dia)
            except:
                pass
        except:
            pass

        print('depois -------> '+ str(auxMateria))
        print('depois ----> ' + str(quantidadeAula))
    print(verticeProf)
    print(verticeMateria)
    print(aulas)
    print(len(aulas))

def main():
    auxDiasSemana = ['segunda','terca','quarta','quinta','sexta']
    for dia in auxDiasSemana:
        print(dia.upper())
        agendaAula(dia)




# FUNCOES
# print(buscarVertice('redes'))
# print(melhoresProfDia('segunda'))
# melhoresProfDia('quarta')
# agendaAula()
main()