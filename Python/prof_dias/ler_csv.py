import csv


grafo = { 'dias' : [], 'professores' : [], 'materias' : [] }
csventrada = 'aulacerta.csv'


def busca_dia(diaDisponivel):
    res = [dias for dias in grafo['dias'] if dias["dia"] == diaDisponivel]
    return res
def busca_professor(professor):
    res = [prof for prof in grafo['professores'] if prof["Nome"] == professor]
    return res

def busca_materia(materia):
   res = [ma for ma in grafo['materias'] if ma['Materia'] == materia]
   return res

def montarGrafo(csventrada):

    with open(csventrada, encoding='utf-8-sig') as ficheiro:
        reader = csv.reader(ficheiro, delimiter=';')
        for row in reader:
            professor = row[0]
            materia = row[1]
            numeroDeAulas = row[2]
            diaDisponivel = row[3]
            hora_1 = row[4]
            hora_2 = row[5]

            dias = busca_dia(diaDisponivel)
            if not dias:
                #{'segunda':{'aulas':[], 'professores-disponiveis':['eloisa','faulin']}}
                grafo['dias'].append({"dia": diaDisponivel, "aulas":[], "professores-disponiveis": [professor]})
            else:
                dias[0]["professores-disponiveis"].append(professor)

            prof = busca_professor(professor)
            if not prof:
                #{'eloisa':{'leciona':'ingles','disonibilidade':{'segunda':[1,2,3,4],'terca':[1,2,3,4],'quinta':[1,2,3,4]}}}
                grafo['professores'].append({"Nome":professor, "Dados": {"leciona": materia,"disonibilidade": {diaDisponivel: [hora_1,hora_2]}}})
            else:
                prof[0]["Dados"]["disonibilidade"].update({diaDisponivel: [hora_1, hora_2]}) 

            ma = busca_materia(materia)
            if not ma:
                #{'ingles':{'quantidade_aula': 2, 'leciondado-por':'eloisa'}}
                grafo['materias'].append({"Materia": materia,   "Dados": {"quantidade_aula": numeroDeAulas, "leciondado-por": professor}})
            else:
                pass

montarGrafo(csventrada)
print(grafo)