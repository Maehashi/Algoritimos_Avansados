import csv

grafo = []

def busca_professor(professor):
   res = [prof for prof in grafo if prof["Nome"] == professor]
   return res

with open('aulacerta.csv', encoding='utf-8-sig') as ficheiro:
        reader = csv.reader(ficheiro, delimiter=';')
        for row in reader:
            professor = row[0]
            materia = row[1]
            numeroDeAulas = row[2]
            diaDisponivel = row[3]
            hora_1 = row[4]
            hora_2 = row[5]

            if "13" == hora_1 and "17" == hora_2:
                horas = "1,2,3,4"

            if "13" == hora_1 and "15" == hora_2:
                horas = "1,2"   

            if "15" == hora_1 and "17" == hora_2:
                horas = "3,4" 

            prof = busca_professor(professor)
            if not prof:
                #{'eloisa':{'leciona':'ingles','disonibilidade':{'segunda':[1,2,3,4],'terca':[1,2,3,4],'quinta':[1,2,3,4]}}}
                grafo.append({"Nome":professor, "Dados": {"leciona": materia,"disonibilidade": {diaDisponivel: [horas]}}})
            else:
                prof[0]["Dados"]["disonibilidade"].update({diaDisponivel: [horas]})
            print(grafo)    
