import csv

grafo = []

def busca_dia(diaDisponivel):
   res = [dias for dias in semana if dias["dia"] == diaDisponivel]
   return res

with open('aulacerta.csv', encoding='utf-8-sig') as ficheiro:
        reader = csv.reader(ficheiro, delimiter=';')
        for row in reader:
            professor = row[0]
            materia = row[1]
            numeroDeAulas = row[2]
            diaDisponivel = row[3]
            hora_1 = row[4]
            hora_2 = row[5]

            dias = busca_dia(diaDisponivel)
            if not dias:
                #{'segunda':{'aulas':[], 'professores-disponiveis':['eloisa','faulin']}}
                grafo.append({"dia": diaDisponivel, "aulas":[], "professores-disponiveis": [professor]})
            else:
                dias[0]["professores-disponiveis"].append(professor)
print(semana)